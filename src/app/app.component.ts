import { Component, OnInit } from '@angular/core';
import { ApiService } from './services/api.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ApiService]
})

export class AppComponent implements OnInit {
  title = 'app';
  users;
  selectedUser;
  activePage = this.apiService.activePage;

  constructor(
    private apiService: ApiService
  ) {}

  ngOnInit() {
    this.apiService.getUsers(this.activePage).subscribe(resp => {
      this.users = resp['data'];
    });
  }

  selectUser(id) {
    this.apiService.getUser(id).subscribe(resp => {
      this.selectedUser = resp['data'];
    });
  }

  pagPrev() {
    this.apiService.activePage = this.activePage === 1 ? this.activePage : this.activePage - 1;
    this.apiService.getUsers(this.apiService.activePage).subscribe(resp => {
      this.users = resp['data'];
      this.activePage = this.apiService.activePage;
    });
  }

  pagNext() {
    this.apiService.activePage = this.activePage + 1;
    this.apiService.getUsers(this.apiService.activePage).subscribe(resp => {
      this.users = resp['data'];
      this.activePage = this.apiService.activePage;
    });
  }
}
