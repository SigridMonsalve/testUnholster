import { HttpClient, HttpParams} from '@angular/common/http';
import { OnInit, Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';

@Injectable()

export class ApiService implements OnInit {
  activePage = 1;

  constructor(
    private http: HttpClient
  ) {}

  ngOnInit() {
  }

  getUsers(pageNum) {
    const pagination = new HttpParams()
      .set('page', pageNum);
    return this.http.get('https://reqres.in/api/users', {params: pagination}).pipe(tap(resp => resp));
  }

  getUser(id) {
    return this.http.get(`https://reqres.in/api/users/${id}`).pipe(tap(resp => resp));
  }
}
