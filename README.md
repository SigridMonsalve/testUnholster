# TestUnholster
I made available this url `http://c62e70e1.ngrok.io` so you can access the instance I have running on my pc.


## Development server

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.
To run this you need to have Angular and node installed. Then:

Run `npm install` to add dependencies used in this project.
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.
